FROM nginx:1.25-alpine

COPY public/ /usr/share/nginx/html

RUN chmod -R a+rwX /usr/share/nginx/html