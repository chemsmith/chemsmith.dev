Title: About

Hi there and welcome to chemsmith.dev! My name is Dylan Smith, and I have spent the better part of my life mixing things together in a laboratory and hoping they don't go bang, or poison me, or both. I worked on research questions in the area of chemical biology where I probed behaviors of the human immune system though the chemical synthesis of small molecules. 

More recently I've moved into the software development and platform engineering space, something I've always had a passion for but didn't pursue as a first career. I've been working in the DevOps space for the last 3 years and as a backend enginner for >5 years. I'm currently working as a Senior Platform Engineer at [catch.com.au](https://www.catch.com.au). 

This blog was originally created in the hope that it would encourage me to write more about the stuff I'm working on. As the front-page can attest, this wasn't as successful as I would have liked. The blog itself is generated using [Pelican](https://getpelican.com); it's then hosted in a simple nginx container, exposed to the world via traefik in a kubernetes cluster running in Oracles Cloud (previously hosted on GitLab Pages). Blog code can be found [here](https://gitlab.com/chemsmith/chemsmith.dev).

Please get in contact via email: [dylan@chemsmith.dev](mailto:dylan@chemsmith.dev)

