#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Dylan Smith'
SITENAME = 'ChemSmith'
SITEURL = ''
TIMEZONE = 'Australia/Melbourne'

PATH = 'content'
OUTPUT_PATH = 'public'
STATIC_PATHS = ['images', 'files']
ARTICLE_PATHS = ['blog']
ARTICLE_SAVE_AS = '{date:%Y}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{slug}.html'
DEFAULT_PAGINATION = 6

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = (
    ('GitLab', 'https://gitlab.com/chemsmith'),
    ('LinkedIn', 'https://www.linkedin.com/in/chemsmith2'),
)

LINKS = (
    ('Home','/'),
    ('About','/pages/about.html'),
    ('CV','https://gitlab.com/chemsmith/cv/-/jobs/artifacts/master/raw/cv.pdf?job=build'),
)

### Voce
THEME = 'voce'

USER_LOGO_URL = '/images/logo.png'
TAGS_URL = 'tags.html'
ARCHIVES_URL = 'archives.html'
###

# MATOMO_URL = 'https://chemsmithdev.matomo.cloud/'
